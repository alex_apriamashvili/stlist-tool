//
//  ParameterLoaderTests.swift
//  stlist-project
//
//  Created by Alex Apriamashvili on 30/9/19.
//

import XCTest
@testable import Core

final class ParameterLoaderTests: CoreTestCase {

  // MARK: - Success Scenario

  func testNoneParametersProvided() {
    let expectedParameters: ParameterLoader.ParameterList = [ConsoleIO.InputParam.none : ""]
    let sut = initSUT(C.bin)
    XCTAssertResultEqual(expectedParameters, sut.loadParameters())
  }

  func testShortHelpParameterProvided() {
    let expectedParameters: ParameterLoader.ParameterList = [ConsoleIO.InputParam.help : ""]
    let sut = initSUT(C.bin, C.helpShort)
    XCTAssertResultEqual(expectedParameters, sut.loadParameters())
  }

  func testLongHelpParameterProvided() {
    let expectedParameters: ParameterLoader.ParameterList = [ConsoleIO.InputParam.help : ""]
    let sut = initSUT(C.bin, C.helpLong)
    XCTAssertResultEqual(expectedParameters, sut.loadParameters())
  }

  func testHelpParameterProvidedAmongOtherParameters() {
    let expectedParameters: ParameterLoader.ParameterList = [ConsoleIO.InputParam.help : ""]
    let sut = initSUT(C.bin, C.helpShort, C.pathLong, C.path)
    XCTAssertResultEqual(expectedParameters, sut.loadParameters())
  }

  func testShortPathParameterProvided() {
    let sut = initSUT(C.bin, C.pathShort, C.path)
    let expectedValue: ParameterLoader.ParameterList = [ConsoleIO.InputParam.path : C.path]
    XCTAssertResultEqual(expectedValue, sut.loadParameters())
  }

  func testLongPathParameterProvided() {
    let sut = initSUT(C.bin, C.pathLong, C.path)
    let expectedValue: ParameterLoader.ParameterList = [ConsoleIO.InputParam.path : C.path]
    XCTAssertResultEqual(expectedValue, sut.loadParameters())
  }

  func testShortOutputParameterProvided() {
    let sut = initSUT(C.bin, C.pathShort, C.path, C.outputShort, C.otherPath)
    let expectedValue = C.otherPath
    XCTAssertEqual(expectedValue, sut.loadParameters().successValueForKey(.output))
  }

  func testLongOutputParameterProvided() {
    let sut = initSUT(C.bin, C.pathLong, C.path, C.outputLong, C.otherPath)
    let expectedValue = C.otherPath
    XCTAssertEqual(expectedValue, sut.loadParameters().successValueForKey(.output))
  }

  func testXCTestRunParameterProvided() {
    let sut = initSUT(C.bin, C.xctestrun, C.path)
    let expectedValue: ParameterLoader.ParameterList = [ConsoleIO.InputParam.xctestrun : C.path]
    XCTAssertResultEqual(expectedValue, sut.loadParameters())
  }

  func testAllParametersProvided() {
    let sut = initSUT(C.bin, C.pathLong, C.path, C.xctestrun, C.otherPath, C.outputLong, C.anotherPath)
    let expectedPath = C.path
    let expectedXCTestRunPath = C.otherPath
    let expectedOutputPath = C.anotherPath
    let result = sut.loadParameters()
    XCTAssertEqual(3, result.successCount())
    XCTAssertEqual(expectedPath, result.successValueForKey(.path)!)
    XCTAssertEqual(expectedXCTestRunPath, result.successValueForKey(.xctestrun)!)
    XCTAssertEqual(expectedOutputPath, result.successValueForKey(.output)!)
  }

  func testAllParametersProvidedAndSwapped() {
    let sut = initSUT(C.bin, C.outputLong, C.anotherPath, C.xctestrun, C.otherPath, C.pathLong, C.path)
    let expectedPath = C.path
    let expectedXCTestRunPath = C.otherPath
    let expectedOutputPath = C.anotherPath
    let result = sut.loadParameters()
    XCTAssertEqual(3, result.successCount())
    XCTAssertEqual(expectedPath, result.successValueForKey(.path)!)
    XCTAssertEqual(expectedXCTestRunPath, result.successValueForKey(.xctestrun)!)
    XCTAssertEqual(expectedOutputPath, result.successValueForKey(.output)!)
  }

  // MARK: - Failure Scenario

  func testNoArgumentsAtAllFailureType() {
    let sut = initSUT()
    XCTAssertResultEqual(InputError.emptyArgumentList, sut.loadParameters())
  }

  func testIncorrectShortHelpParameterProvided() {
    let sut = initSUT(C.bin, C.helpShortIncorrect)
    XCTAssertResultEqual(InputError.unevenNumberOfParameters, sut.loadParameters())
  }

  func testIncorrectLongHelpParameterProvided() {
    let sut = initSUT(C.bin, C.helpLongIncorrect)
    XCTAssertResultEqual(InputError.unevenNumberOfParameters, sut.loadParameters())
  }

  func testUnevenNumberOfParametersProvided() {
    let sut = initSUT(C.bin, C.pathLong, C.path, C.pathShort)
    XCTAssertResultEqual(InputError.unevenNumberOfParameters, sut.loadParameters())
  }

  func testInvalidParameterSequenceRecognized() {
    let sut = initSUT(C.bin, C.pathLong, C.pathShort, C.path, C.path)
    XCTAssertResultEqual(InputError.invalidParameterSequence, sut.loadParameters())
  }

  func testInvalidParameterSequenceRecognizedReversed() {
    let sut = initSUT(C.bin, C.pathLong, C.path, C.path, C.pathShort)
    XCTAssertResultEqual(InputError.invalidParameterSequence, sut.loadParameters())
  }

  func testUnrecognizedParameterFound() {
    let sut = initSUT(C.bin, C.validUnrecognizedKey, C.path)
    XCTAssertResultEqual(InputError.unrecognizedParameter, sut.loadParameters())
  }

  func testShallFailIfOnlyOutputWasSpecified() {
    let sut = initSUT(C.bin, C.outputShort, C.path)
    XCTAssertResultEqual(InputError.invalidParameterSequence, sut.loadParameters())
  }
}

// MARK: - Helpers

private extension ParameterLoaderTests {

  func arg(_ a: [String]) -> (list: [String], count: Int32) {
    return (list: a, count: Int32(a.count))
  }

  func initSUT(_ args: String...) -> ParameterLoader {
    let a = arg(args)
    return ArgumentLoader(a.list, a.count)
  }

  enum C {
    static let bin = "stlist"
    static let helpShort = "-h"
    static let helpShortIncorrect = "h"
    static let helpLong = "--help"
    static let helpLongIncorrect = "--hwlp"
    static let pathShort = "-p"
    static let pathLong = "--path"
    static let xctestrun = "--xctestrun"
    static let outputShort = "-o"
    static let outputLong = "--output"
    static let path = "./some/path"
    static let otherPath = "./some/other/path"
    static let anotherPath = "./another/path"
    static let validUnrecognizedKey = "--unrecognized"
  }
}

extension Result where Success == ParameterLoader.ParameterList {

  func successValueForKey(_ key: ParameterLoader.ParameterList.Key) -> ParameterLoader.ParameterList.Value? {
    guard case let .success(dictionary) = self else { return nil }
    return dictionary[key]
  }
}

