//
//  MockParameterLoader.swift
//  CoreTests
//
//  Created by Alex Apriamashvili on 2/10/19.
//

import Core

final class MockParameterLoader: ParameterLoader, MockInvocable {

  enum Invocation: String, MethodInvocation {
    case loadParameters
  }

  var invocationList: [Invocation] = []
  private let stubbedLoaderResult: Result<ParameterLoader.ParameterList, InputError>

  init(_ result: Result<ParameterLoader.ParameterList, InputError> = .success([:])) {
    stubbedLoaderResult = result
  }

  static func faulty(_ error: InputError) -> MockParameterLoader {
    return MockParameterLoader(.failure(error))
  }

  static func successful(_ parameters: ParameterLoader.ParameterList) -> MockParameterLoader {
    return MockParameterLoader(.success(parameters))
  }

  func loadParameters() -> Result<ParameterLoader.ParameterList, InputError> {
    invocationList.append(.loadParameters)
    return stubbedLoaderResult
  }
}
