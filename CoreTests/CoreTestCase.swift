//
//  CoreTestCase.swift
//  CoreTests
//
//  Created by Alex Apriamashvili on 30/9/19.
//

import XCTest

open class CoreTestCase: XCTestCase {  }

// MARK: - Invocations

public protocol MockInvocable {

  associatedtype Invocation

  var invocationList: Array<Invocation> { get }
}

public protocol MethodInvocation: Equatable, RawRepresentable {}

public extension MethodInvocation where RawValue == String {

  var rawValue: String {
    return "\(self)"
  }
}

// MARK: - Custom Assertions

/// assert result to be successful
/// - parameter expression: an expression that is asserted to be successful
/// - parameter message: additional failure message
/// - parameter file: assertion test file
/// - parameter line: assertion line number
public func XCTAssertResultSuccess<Success, Failure>(_ expression: @autoclosure () -> Result<Success, Failure>,
                                                     _ message: @autoclosure () -> String = "",
                                                     file: StaticString = #file,
                                                     line: UInt = #line) {
  XCTAssertNotNil(expression().success(), message(), file: file, line: line)
}

/// assert result to be faulty
/// - parameter expression: an expression that is asserted to be faulty
/// - parameter message: additional failure message
/// - parameter file: assertion test file
/// - parameter line: assertion line number
public func XCTAssertResultFailure<Success, Failure>(_ expression: @autoclosure () -> Result<Success, Failure>,
                                                     _ message: @autoclosure () -> String = "",
                                                     file: StaticString = #file,
                                                     line: UInt = #line) {
  XCTAssertNotNil(expression().failure(), message(), file: file, line: line)
}

/// assert result to be successful and equal to the value specified
/// - parameter expectedValue: expected contents of the `.success(T)` case
/// - parameter expression: an expression that is asserted to be successful
/// - parameter message: additional failure message
/// - parameter file: assertion test file
/// - parameter line: assertion line number
public func XCTAssertResultEqual<Success: Equatable, Failure>(_ expectedValue: @autoclosure () -> Success,
                                                              _ expression: @autoclosure () -> Result<Success, Failure>,
                                                              _ message: @autoclosure () -> String = "",
                                                              file: StaticString = #file,
                                                              line: UInt = #line) {
  guard let value = expression().success() else { return XCTAssert(false, message(), file: file, line: line) }
  XCTAssertEqual(expectedValue(), value, message(), file: file, line: line)
}

/// assert result to be faulty and equal to a specific error
/// - parameter expectedFailure: expected contents of the `.failure(T)` case
/// - parameter expression: an expression that is asserted to be faulty
/// - parameter message: additional failure message
/// - parameter file: assertion test file
/// - parameter line: assertion line number
public func XCTAssertResultEqual<Success, Failure: Equatable>(_ expectedFailure: @autoclosure () -> Failure,
                                                              _ expression: @autoclosure () -> Result<Success, Failure>,
                                                              _ message: @autoclosure () -> String = "",
                                                              file: StaticString = #file,
                                                              line: UInt = #line) {
  guard let failure = expression().failure() else { return XCTAssert(false, message(), file: file, line: line) }
  XCTAssertEqual(expectedFailure(), failure, message(), file: file, line: line)
}

/// assert that a specific invocation was called for an instance for a given number of times
/// - parameter invocation: an invocation that is expected to be called
/// - parameter instance: an instance that is expected to contain the invocation(s)
/// - parameter numberOfCalls: number of times the above-mentioned invocation is expected to be called on the  instance
/// - parameter message: additional failure message
/// - parameter file: assertion test file
/// - parameter line: assertion line number
public func XCTAssertInvocationIsCalled<M: MockInvocable, I: MethodInvocation>(_ invocation: @autoclosure () -> I,
                                                                               _ instance: @autoclosure () -> M,
                                                                               _ numberOfCalls: Int = 1,
                                                                               _ message: @autoclosure () -> String = "",
                                                                               file: StaticString = #file,
                                                                               line: UInt = #line) {
  let invocationList = instance().invocationList
  var count = 0
  for i in invocationList {
    guard let inv = i as? I else { XCTFail(message(), file: file, line: line); return }
    if inv == invocation() { count += 1 }
  }
  XCTAssertEqual(count, numberOfCalls, message(), file: file, line: line)
}

// MARK: - Result<Success, Failure> Categories

public extension Result where Success: Collection {

  func successCount() -> Int {
    guard case let .success(collection) = self else { return 0 }
    return collection.count
  }
}

public extension Result {

  func success() -> Success? {
    guard case let .success(value) = self else { return nil }
    return value
  }

  func failure() -> Failure? {
    guard case let .failure(value) = self else { return nil }
    return value
  }
}
