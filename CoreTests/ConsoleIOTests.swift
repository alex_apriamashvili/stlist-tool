//
//  ConsoleIOTests.swift
//  CoreTests
//
//  Created by Alex Apriamashvili on 2/10/19.
//

import XCTest
@testable import Core

final class ConsoleIOTests: CoreTestCase {

  private var loader: MockParameterLoader!
  private var sut: ConsoleIO!

  override func setUp() {
    super.setUp()
    loader = MockParameterLoader()
    sut = ConsoleIO(loader)
  }

  override func tearDown() {
    sut = nil
    loader = nil
    super.tearDown()
  }

  func testLoaderIsBeingCalled() {
    _ = sut.inputParameters()
    XCTAssertInvocationIsCalled(MockParameterLoader.Invocation.loadParameters, loader)
  }

  func testNoParametersLoadedIfError() {
    loader = MockParameterLoader.faulty(.invalidParameterSequence)
    sut = ConsoleIO(loader)
    let result = sut.inputParameters()
    XCTAssert(result.isEmpty)
  }

  func testOnlyPathParameterReturned() {
    loader = MockParameterLoader.successful([.path: C.path])
    sut = ConsoleIO(loader)
    let result = sut.inputParameters()
    XCTAssertEqual(1, result.count)
    XCTAssertEqual(.path, result[0].key)
    XCTAssertEqual(C.path, result[0].value)
  }

  func testPathParameterPreoritisedOverFilter() {
    loader = MockParameterLoader.successful([.xctestrun: C.path, .path: C.path])
    sut = ConsoleIO(loader)
    let result = sut.inputParameters()
    XCTAssertEqual(2, result.count)
    XCTAssertEqual(.path, result[0].key)
    XCTAssertEqual(.xctestrun, result[1].key)
  }

  func testOutputParameterDepreoritised() {
    loader = MockParameterLoader.successful([
        .output: C.path,
        .path: C.path,
        .xctestrun: C.path
      ])
    sut = ConsoleIO(loader)
    let result = sut.inputParameters()
    XCTAssertEqual(3, result.count)
    XCTAssertEqual(.path, result[0].key)
    XCTAssertEqual(.xctestrun, result[1].key)
    XCTAssertEqual(.output, result[2].key)
  }
}

private extension ConsoleIOTests {

  enum C {
    static let path = "./some/path"
  }
}
