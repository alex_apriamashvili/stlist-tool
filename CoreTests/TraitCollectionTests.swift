//
//  TraitCollectionTests.swift
//  CoreTests
//
//  Created by Alex Apriamashvili on 2/10/19.
//

import XCTest
import Core

class TraitCollectionTests: CoreTestCase {

  // MARK: - Grep

  func testGrepSingleEntry() {
    let pattern = "\\d+x\\d+"
    let result = C.Grep.haystack.grep(pattern)
    XCTAssertFalse(result.isEmpty)
    XCTAssertEqual("09x083", result.first)
  }

  func testGrepMultipleEntries() {
    let pattern = "\\d*x\\d*"
    let result = Set(C.Grep.haystack.grep(pattern))
    XCTAssertFalse(result.isEmpty)
    XCTAssertEqual(2, result.count)
    XCTAssert(result.contains("09x083"))
    XCTAssert(result.contains("x"))
  }

  func testGrepNoEntry() {
    let pattern = "AAAbbZZss"
    let result = C.Grep.haystack.grep(pattern)
    XCTAssert(result.isEmpty)
  }

  func testGrepEmptyString() {
    let pattern = "AAAbbZZss"
    let result = String().grep(pattern)
    XCTAssert(result.isEmpty)
  }

  func testPerformanceGrep() {
    self.measure {
      _ = C.Grep.hugeHaystack.grep(C.Grep.haystack)
    }
  }

  // MARK: - IsSuccessfulShellExecution

  func testIsSuccessfulShellExecutionSuccess() {
    let returnCode: Int32 = 0
    XCTAssert(returnCode.isSuccess())
  }

  func testIsSuccessfulShellExecutionFailure() {
    let returnCode: Int32 = 1
    XCTAssertFalse(returnCode.isSuccess())
  }

  // MARK: - UniqueSorted

  func testUnique() {
    let result = C.UniqueSorted.origin.uniqueSorted()
    XCTAssertEqual(4, result.count)
    XCTAssertEqual(1, result.filter { $0 == "AB" }.count)
  }

  func testSorted() {
    let result = C.UniqueSorted.origin.uniqueSorted()
    XCTAssertEqual(["A", "AB", "BC", "CD"], result)
  }

  func testPerformanceUniqueSorted() {
    self.measure {
      _ = C.UniqueSorted.hugeOrigin.uniqueSorted()
    }
  }

  // MARK: - Strip

  func testStripping() {
    XCTAssertEqual(C.Strip.stripped, C.Strip.coated.strip())
  }

  func testStrippedUnchainged() {
    XCTAssertEqual(C.Strip.stripped, C.Strip.stripped.strip())
  }

  // MARK: - ToTest

  func testFormattedStringTurnedtoTest() {
    let result = C.ToTest.rawTestString.toTest()
    XCTAssertNotNil(result)
    XCTAssertEqual(C.ToTest.test, result!)
  }

  func testNonFormattedStringTurnedtoNil() {
    let result = C.ToTest.incorrectTestString.toTest()
    XCTAssertNil(result)
  }

  // MARK: - ToString

  func testTestToString() {
    XCTAssertEqual(C.ToString.testString, C.ToString.test.toString())
  }
}

private extension TraitCollectionTests {

  enum C {

    enum Grep {
      static let haystack = "aaBbcdzxyesDDgf435 kjdj 09x083 lnn84nf03 ADFFxEFFF"
      static let hugeHaystack = Array(repeating: C.Grep.haystack, count: 100000).joined()
    }

    enum UniqueSorted {
      static let origin = ["A", "AB", "BC", "AB", "CD"]
      static let hugeOrigin = Array(repeating: C.UniqueSorted.origin, count: 100000).flatMap { $0 }
    }

    enum Strip {
      static let coated = "\nsome string here\n\n"
      static let stripped = "some string here"
    }

    enum ToTest {
      static let rawTestString = "TestTarget.TestClass.testMethod"
      static let incorrectTestString = "TestTarget/TestClass_testMethod"
      static let test = Test("TestTarget", "TestClass", "testMethod")
    }

    enum ToString {
      static let test = C.ToTest.test
      static let testString = C.ToTest.rawTestString
    }
  }
}
