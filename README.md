# stlist (Swift Test List)

A utility written in `Swift` that helps to obtain a list of tests by demangling xctest binaries.

### >_ Usage
Currently, the tool is supporting test list extraction along with the filtration of the skipped tests according to the  `.xctestrun` file provided. 
Below is the list of parameters  currently supported by the tool:
  - **`-h`** or **`--help`** – prints out the help message with the list of parameters supported for that version; 
  - **`-p`** or **`--path`** – a parameter that helps to specify the nearest path to all test binaries contained inside `.xctest` bundles. For example, a path to all `.app` files inside build products ( `~/Library/Developer/Xcode/DerivedData/Build/Products/Testing-iphonesimulator`);
  - **`--xctestrun`** – a parameter that helps to specify the exact location of the `.xctestrun` file for the intended test run;
  - **`-o`** or **`--output`** – a parameter that helps to specify the output folder for the final extracted test list. _If not specified, the test list could be retrieved from the console output, however, that output might contain some redundant information in a form of errors and warnings that might make it harder to parse the list. It is preferred to specify this parameter while using the tool_
Example:
```bash
$ stlist \
  --path /Library/Developer/Xcode/DerivedData/Build/Products/Testing-iphonesimulator \
  --xctestrun /Library/Developer/Xcode/DerivedData/Build/Products/Tests_iphonesimulator12.4-x86_64.xctestrun \
  -o ./
```
Once the execution is finished you could find your test list under: `./stlist-out/test_list`.  
If you wish to have a pure test list output in your terminal window, please, use the following script:
or 
```bash
$ stlist \
  --path /Library/Developer/Xcode/DerivedData/Build/Products/Testing-iphonesimulator \
  --xctestrun /Library/Developer/Xcode/DerivedData/Build/Products/Tests_iphonesimulator12.4-x86_64.xctestrun \
  -o ./
$ clear
$ cat ./stlist-out/test_list
```
Upon completion you will see a test list in the following format:
```text
TestTarget.TestClass.testMethodA
TestTarget.TestClass.testMethodB
TestTarget.TestClass.testMethodC
...
TestTarget.TestClass.testMethodZ
```
The test list is sorted in alphanumeric order.

### ⚠️ Warning
This tool is using swift standard library `libswiftDemangle` to perform demangling.  
Please, ensure that you have have this library under:
```
/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/libswiftDemangle.dylib
```

### ✨ Contribution
All kinds of contribution is highly encouraged.  
Please, see [Evolution](#evolution) section for more information.  
To make modifications in the code base, please, generate the `.xcodeproj` file, for your convenience.   
That could be done by executing the following command:
```bash
swift package generate-xcodeproj
```

### 🛠 Compilation & Testing 
At the moment there 2 ways of compiling the project: using SPM (Swift Package Manager) and using Bazel.

1. **Bazel**  
Before starting the compilation, please, ensure that you have Bazel of version not less than `0.29.1`  is installed on your environment.  
The information on how to install Bazel could be found [here](https://docs.bazel.build/versions/master/install-os-x.html).
To compile the project with Bazel, please, execute the command in the root folder of your working copy:
```bash
bazel build //:stlist
```
Once the command is finished, you shall be able to find a binary under: `./bazel-bin/stlist`

If you feel like contributing to the project, please, don't forget to test your code.  
To test code with Bazel simply execute:
```bash
bazel test //:core_tests
```
this command will run tests included in the `CoreTests` target. Shall you create a new target, please, don't forget to update Bazel `BUILD` file.

2. **SPM**  
Similarily to Bazel, you might use SPM to compile the project and, unlikely to Bazel, you don't need to download any additional dependencies on your machine, as `swift-build` is included in your `swift.xctoolchain` already.  
To compile, please use the following command:
```bash
swift build
```
After a successful execution of that command, the binary could be found at:  `./.build/x86_64-apple-macosx/debug/stlist`
To run tests with SPM, siply execute:
```bash
swift test
```

### 🦠 Evolution
The utility now has its basic functionality but that functionality still has room to improve as well as new features are welcome.  
Please, see [Issues](https://bitbucket.org/alex_apriamashvili/stlist-tool/issues) for more features that are planned for this tool.  
Didn't find the feature you would like to have in this tool, please, feel free to create your own issue and mark its kind as `proposal`.  
Found a bug that needs to be addressed? Please, fill up an issue by specifying it as `bug`. 
