import Foundation
import Core
import Filter

final class CLI {

  private var shell: ShellGateway!
  private var demangler: Demangler!
  private var binaryFinder: BinaryFinder!
  private var nm: NameMangling!
  private var fileHelper: FileHelper!

  private var cachedTestList = [Test]()

  func main() {
    let input = ConsoleIO().inputParameters()
    guard !input.isEmpty else { showNoParamsErrorAndHelp(); return }
    guard let h = input.first?.key, h != .help else { showHelp(); return }
    guard let noParam = input.first?.key, noParam != .none else { showNoParamsErrorAndHelp(); return }
    for (key: key, value: value) in input {
      switch key {
      case .path: obtainTestList(value)
      case .xctestrun: filterTestList(value, cachedTestList)
      case .output: moveToOutput(value)
      default: break
      }
    }
    cachedTestList.forEach { print($0.toString()) }
    cachedTestList = [Test]()
  }
}

// MARK: - Execution

private extension CLI {

  func obtainTestList(_ path: String) {
    buildDependencies()
    measure("obtainTestList") { [weak self] () -> Void in
      guard let it = self else { return }
      it.fileHelper.removeAllTempFiles()
      it.fileHelper.createTemporaryDirectoryIfNeeded()
      it.binaryFinder.binaryList(at: path).forEach { it.nm.mangle("\(path)/\($0)", filter: C.testSymbolRegEx) }
      it.fileHelper
        .readFile(at: it.nm.mangleBin)
        .map { it.demangler.demangle($0)?.extractFullTestName() }
        .compactMap { $0 }
        .uniqueSorted()
        .forEach { if let test = $0.toTest() { it.cachedTestList.append(test) } }
      let testStrings = it.cachedTestList.map { $0.toString() }
      let unfilteredTestFilepath = "\(it.fileHelper.tempDir)/\(C.testListFilename)"
      try? it.fileHelper.write(testStrings, file: unfilteredTestFilepath)
      it.fileHelper.removeFile(at: it.nm.mangleBin)
    }
  }

  func filterTestList(_ xctestrunPath: String, _ cache: [Test] = []) {
    let origin = savedTestList(cache)
    measure("filterTestList") { [weak self] () -> Void in
      guard let it = self else { return }
      let xctestrunResult = IOSXCTestRun.load(at: xctestrunPath)
      switch xctestrunResult {
      case let .success(xctestrun):
        let skipList = xctestrun.skippedTestList()
        let filtered = TestFilter.filter(origin.map{ $0.toString() }, by: skipList)
        try? it.fileHelper.write(filtered, file: "\(it.fileHelper.tempDir)/\(C.testListFilename)")
      default:
        ConsoleIO.writeMessage("unable to read xctestrun file", to: .error)
      }
    }
  }

  func moveToOutput(_ output: String) {
    measure("moveToOutput") {[weak self] () -> Void in
      guard let it = self else { return }
      it.fileHelper.removeFile(at: "\(output)/\(it.fileHelper.outputDirName)")
      it.fileHelper.moveTempDirTo(output)
      ConsoleIO.writeMessage("\(C.MSG.customOutput)\(output)/\(it.fileHelper.outputDirName)")
    }
  }

  private func buildDependencies() {
    measure("buildDependencies") { [weak self] () -> Void in
      guard let it = self else { return }
      it.shell = ShellGatewayImpl()
      it.fileHelper = FileHelperImpl(it.shell)
      it.binaryFinder = TestBinaryFinder(it.fileHelper)
      it.nm = XCRunMangler(it.shell, it.fileHelper)
      it.demangler = LibSwiftDemangle()
    }
  }

  private func savedTestList(_ cache: [Test]) -> [Test] {
    return measure("savedTestList") { [weak self] () -> [Test] in
      guard let it = self else { return [] }
      var list = cache
      if list.isEmpty {
        let stash = "\(it.fileHelper.tempDir)/\(C.testListFilename)"
        list = it.fileHelper.readFile(at: stash).compactMap { $0.toTest() }
      }
      return list
    }
  }
}

// MARK: - Help

private extension CLI {

  private func showHelp() {
    ConsoleIO.writeMessage(C.MSG.help)
  }
}

// MARK: - Error

private extension CLI {

  private func showNoParamsErrorAndHelp() {
    ConsoleIO.writeMessage(C.MSG.noParamError, to: .error)
    ConsoleIO.writeMessage(C.MSG.help)
    exit(1)
  }
}

// MARK: - Helpers

private extension String {

  func extractFullTestName() -> String {
    return self
      .grep(C.testNameRegEx)
      .first?
      .replacingOccurrences(of: "(", with: "") ?? ""
  }
}

private struct C {
  static let testNameRegEx = "(\\w+\\.)+test\\w+\\("
  static let testSymbolRegEx = ".*\\sT\\s.*test"
  static let testListFilename = "test_list"

  struct MSG {
    static let customOutput = "Your test list could befound in: "
    static let noParamError = "Input error: Please make sure that specified input is correct."
    static let help = """

Please, use the following input:

-p, --path /path/to/binaries – to obtain a list of tests for a given path

-h, --help – to see help message

--xctestrun – path to the .xctestresult file to filter out the obtained test list

-o, --output – path to the directory that the result shall be saved under


"""
  }
}

CLI().main()
