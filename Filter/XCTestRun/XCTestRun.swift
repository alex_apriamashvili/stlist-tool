//
//  XCTestRun.swift
//  Filter
//
//  Created by Alex Apriamashvili on 1/10/19.
//

import Foundation

public protocol XCTestRun {

  typealias Contents = [String : Any]

  /// list all the skipped tests included into the `.xctestrun` file
  /// - returns: a list of found tests to be filtered
  ///
  /// format:
  /// ```
  /// [
  ///   TestTargetA.TestClassA.testMethodA,
  ///   ...
  ///   TestTargetZ.TestClassZ.testMethodZ,
  /// ]
  /// ```
  func skippedTestList() -> [String]
}

public final class IOSXCTestRun: XCTestRun {

  private let contents: Contents

  init(_ contents: Contents) {
    self.contents = contents
  }

  public static func load(at path: String) -> Result<IOSXCTestRun, XCTestRunError> {
    guard let xmlData = FileManager.default.contents(atPath: path) else { return .failure(.unableToLoadFileForGivenPath) }
    let contents: Contents?
    do {
      contents = try PropertyListSerialization.propertyList(
        from: xmlData,
        options: .mutableContainersAndLeaves,
        format: nil) as? Contents
    } catch {
      return .failure(.unableToLoadFileForGivenPath)
    }
    guard let content = contents else { return .failure(.earlyEOF) }
    let xctestrun = IOSXCTestRun(content)
    return .success(xctestrun)
  }

  public func skippedTestList() -> [String] {
    var skipped = [String]()
    for key in contents.keys {
      guard let target = contents[key] as? Contents,
        let skipList = target[C.skippedTestsKey] as? [String] else { continue }
      skipped.append(contentsOf: skipList.map { "\(key)/\($0)" })
    }
    return skipped.map { $0.replacingOccurrences(of: C.oldSeparator, with: C.newSeparator) }
  }
}

public enum XCTestRunError: Error {

  case unableToLoadFileForGivenPath
  case earlyEOF
}

private extension IOSXCTestRun {

  enum C {
    static let skippedTestsKey = "SkipTestIdentifiers"
    static let oldSeparator = "/"
    static let newSeparator = "."
  }
}
