//
//  TestFilter.swift
//  Filter
//
//  Created by Alex Apriamashvili on 1/10/19.
//

public struct TestFilter {

  public static func filter<T: StringProtocol>(_ origin: [T], by skipList: [T]) -> [T] {
    var filteredList = [T]()
    for test in origin {
      guard !isIn(test, skipList) else { continue }
      filteredList.append(test)
    }
    return filteredList
  }

  private static func isIn<T: StringProtocol>(_ value: T, _ list: [T]) -> Bool {
    for e in list { if value.contains(e) { return true } }
    return false
  }
}
