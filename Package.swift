// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// MARK: - Constants

enum C {

  enum Target {
    static let main = "stlist"
    static let core = "Core"
    static let filter = "Filter"
    static let coreTests = "CoreTests"
    static let filterTests = "FilterTests"
  }
}

// MARK: - Helpers

func rootDir(_ name: String) -> String  {
  return "./\(name)/"
}

protocol TargetDescription {

  var asDependency: Target.Dependency { get }
}

extension Target: TargetDescription {

  var asDependency: Target.Dependency {
    return Target.Dependency(stringLiteral: name)
  }
}

// MARK: - Products

let coreLib = Product.library(name: C.Target.core, type: .static, targets: [C.Target.core])
let filterLib = Product.library(name: C.Target.filter, type: .static, targets: [C.Target.filter])

// MARK: - Production Targets

let core = Target.target(
  name: C.Target.core,
  dependencies: [],
  path: rootDir(C.Target.core)
)

let filter = Target.target(
  name: C.Target.filter,
  dependencies: [],
  path: rootDir(C.Target.filter)
)

let main = Target.target(
  name: C.Target.main,
  dependencies: [
    core.asDependency,
    filter.asDependency,
  ]
)

// MARK: - Test Targets

let coreTests = Target.testTarget(
  name: C.Target.coreTests,
  dependencies: [core.asDependency],
  path: rootDir(C.Target.coreTests)
)

// MARK: - Package

let package = Package(
  name: C.Target.main,
  platforms: [.macOS(.v10_12)],
  products: [
    coreLib,
    filterLib,
  ],
  targets: [
    main,
    core,
    filter,
    coreTests,
  ]
)
