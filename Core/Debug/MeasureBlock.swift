//
//  MeasureBlock.swift
//  stlist
//
//  Created by Alex Apriamashvili on 22/9/19.
//

import Foundation

public typealias MeasureBlock<T> = () -> T

/// measure execution time of a particular block of code
/// the execution time is then printed to a console
///
/// the function takes effect in the DEBUG mode only
///
/// Usage:
/// ```
///   let result = measure("my_awesome_block_of_code") { () -> Int in
///     2 + 2
///   }
/// ```
/// - parameter name: name of the block that will be printed out in the console
/// - parameter block: the block of code itself
/// - returns: whatever the block of code is supposed to return, the result is also discardable
@discardableResult
public func measure<T>(_ name: String? = nil, _ block: @escaping MeasureBlock<T>) -> T {
  #if os(Linux) || !DEBUG
  return block()
  #else
  let start = CFAbsoluteTimeGetCurrent()
  let result = block()
  let end = CFAbsoluteTimeGetCurrent()
  let fn = name ?? ""
  ConsoleIO.writeMessage("\(fn) execution time: \(end - start) seconds")
  return result
  #endif
}
