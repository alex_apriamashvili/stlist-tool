//
//  FileHelper.swift
//  
//
//  Created by Alex Apriamashvili on 16/9/19.
//

import Foundation

public protocol FileHelper {

  /// path to the temporary dir
  var tempDir: String { get }

  /// output dir name
  var outputDirName: String { get }

  /// provide an xctest binary name for a given path
  /// - parameter: path to the .xctest bundle
  /// - returns: an xctest binary name or an empty string if not available
  func testBundleBinaryName(at path: String) -> String

  /// create a temporary directory for stlist under `/tmp` if doesn't exist
  /// - returns: an absolute path to that directory
  @discardableResult
  func createTemporaryDirectoryIfNeeded() -> String

  /// remove file at a given path
  /// - parameter path: path to the file that needs to be removed
  /// - returns: `true` if the removal was successful, otherwise - `false`
  @discardableResult
  func removeFile(at path: String) -> Bool

  /// create file at a given path
  /// - parameter path: path to the file that needs to be created
  /// - returns: `true` if the creation was successful, otherwise - `false`
  @discardableResult
  func touchFile(at path: String) -> Bool

  /// remove all files under the stlist temporary directory by removing the directory itself
  /// - returns: `true` if the removal was successful, otherwise - `false`
  @discardableResult
  func removeAllTempFiles() -> Bool

  /// read file at a given path and return its contents line by line, excluding empty lines as an array
  /// - parameter path: path to the file that needs to be read
  /// - returns: contents of file separated by `/n`
  func readFile(at path: String) -> [String]

  /// write provided contents (array of strings) into a file line-by-line
  /// - parameter contents: an array of strings that needs to be written into a file
  /// - parameter path: path to the file that needs to be written
  /// - throws: an exception if there was an error upon writing the contents
  func write(_ contents: [String], file: String) throws

  /// move contents of the temp directory under a specific path
  /// - parameter path: new path that contents of the temp dir need to be relocated under
  /// - returns: `true` if the relocation was successful, otherwise - `false`
  @discardableResult
  func moveTempDirTo(_ path: String) -> Bool
}

public final class FileHelperImpl: FileHelper {

  public var tempDir: String = C.tempDir
  public var outputDirName: String =  C.outputDir

  private let fileManager = FileManager.default
  private let shell: ShellGateway

  public init(_ shell: ShellGateway) {
    self.shell = shell
  }

  public func testBundleBinaryName(at path: String) -> String {
    return path
    .grep(C.regex)
    .first!
    .replacingOccurrences(of: C.xctestExtension, with: "")
  }

  @discardableResult
  public func createTemporaryDirectoryIfNeeded() -> String {
    var isDir: ObjCBool = true
    guard !fileManager.fileExists(atPath: C.tempDir, isDirectory: &isDir) else { return C.tempDir }
    do {
      try fileManager.createDirectory(atPath: C.tempDir, withIntermediateDirectories: true, attributes: nil)
      return C.tempDir
    } catch {
      print(error.localizedDescription)
    }
    return ""
  }

  @discardableResult
  public func removeAllTempFiles() -> Bool {
    return removeFile(at: C.tempDir)
  }

  public func readFile(at path: String) -> [String] {
    do {
      let data = try String(contentsOfFile: path, encoding: .utf8)
      let lines = data.components(separatedBy: .newlines)
      return lines.filter({ !$0.isEmpty })
    } catch {
      print(error)
      return []
    }
  }

  public func write(_ contents: [String], file: String) throws {
    guard touchFile(at: file) else { throw FileHelperError.unableToCreateFile(file) }
    try contents.joined(separator: C.lineBreak).write(toFile: file, atomically: true, encoding: .utf8)
  }

  @discardableResult
  public func removeFile(at path: String) -> Bool {
    let cmd = "\(C.CMD.remove) \"\(path)\""
    return shell.execute(cmd).isSuccess()
  }

  @discardableResult
  public func touchFile(at path: String) -> Bool {
    let cmd = "\(C.CMD.touch) \"\(path)\""
    return shell.execute(cmd).isSuccess()
  }

  @discardableResult
  public func moveTempDirTo(_ path: String) -> Bool {
    let cmd = "\(C.CMD.move) \"\(tempDir)/\" \"\(path)/\(C.outputDir)\""
    return shell.execute(cmd).isSuccess()
  }
}

private extension FileHelperImpl {

  struct C {
    static let xctestExtension = ".xctest"
    static let regex = "\\w+\\.xctest"
    static let tempDir = "/tmp/com.alex.apriamashvili.stlist"
    static let lineBreak = "\n"
    static let outputDir = "stlist-out"

    struct CMD {
      static let remove = "rm -rf"
      static let touch = "touch"
      static let move = "mv"
      static let echo = "echo"
    }
  }
}

public enum FileHelperError: Error {
  case unableToCreateFile(String)
  case dataConversionFailed
  case unableToOpenFile(String)
}
