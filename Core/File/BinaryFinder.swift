//
//  FileHelper.swift
//  
//
//  Created by Alex Apriamashvili on 16/9/19.
//

import Foundation

public protocol BinaryFinder {

  /// list all xctest binaries found at a given path
  /// the search is recoursive
  /// - parameter path: path to the directory that is supposed to contain xctest binaries
  /// - returns: a list of test binaries found
  func binaryList(at path: String) -> [String]
}

public final class TestBinaryFinder: BinaryFinder {

  private let helper: FileHelper

  public init(_ helper: FileHelper) {
    self.helper = helper
  }

  public func binaryList(at path: String) -> [String] {
    return findTestBundles(at: path).compactMap {
      let binary = helper.testBundleBinaryName(at: $0)
      return "\($0)/\(binary)"
    }
  }
}

private extension TestBinaryFinder {

  func findTestBundles(at path: String) -> [String] {
    var xctestPaths = [String]()
    let fileManager = FileManager.default
    guard let enumerator = fileManager.enumerator(atPath: path) else { return [] }
    while let element = enumerator.nextObject() as? String {
      if element.hasSuffix(C.xctestExtension) {
        xctestPaths.append(element)
      }
    }
    return xctestPaths
  }
}

private extension TestBinaryFinder {

  struct C {
    static let xctestExtension = ".xctest"
  }
}
