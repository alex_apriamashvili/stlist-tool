//
//  DemanglerDylib.swift
//  stlist
//
//  Created by Alex Apriamashvili on 22/9/19.
//

import Foundation
// MARK: - Demangler

/// a protocol that describes an entity that is eligible for demangling symbols
public protocol Demangler {

  /// demangle given string into a human-readable format
  /// - parameter _: a mangled string that needs to be demangled
  /// - returns: a human-readable representation of a string
  func demangle(_: String) -> String?
}

// MARK: - Implementastion (libswiftDemangle)

public final class LibSwiftDemangle: Demangler {

  private let handle: UnsafeMutableRawPointer
  private let internalDemangleFunction: SwiftDemangleFunction

  public init() {
    handle = dlopen(C.libPath, RTLD_NOW)
    let address = dlsym(self.handle, C.libswiftDemangle.swift_demangle_getDemangledName)
    internalDemangleFunction = unsafeBitCast(address, to: SwiftDemangleFunction.self)
  }

  deinit { dlclose(self.handle) }

  public func demangle(_ input: String) -> String? {
    let extractedSymbols = extractSymbols(input)
    let outputString = UnsafeMutablePointer<CChar>.allocate(capacity: C.bufferCapacity)
    let resultSize = self.internalDemangleFunction(extractedSymbols, outputString, C.bufferCapacity)
    guard resultSize <= C.bufferCapacity else { return nil }
    return String(cString: outputString, encoding: .utf8)
  }
}

// MARK: - Private

private extension LibSwiftDemangle {

  func extractSymbols(_ input: String) -> String {
    guard let codeSegmentIdentifierUpperBound = input.range(of: C.codeSegmentIdentifier)?.upperBound else {
      return input
    }
    return String(input[codeSegmentIdentifierUpperBound...])
  }
}

// MARK: - Constant

private extension LibSwiftDemangle {

  private typealias SwiftDemangleFunction =
    @convention(c) (UnsafePointer<CChar>, UnsafeMutablePointer<CChar>, size_t) -> size_t

  struct C {
    private init() {}

    static let codeSegmentIdentifier = " T "

    static let libPath =
    "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/libswiftDemangle.dylib"

    static let bufferCapacity = 1024

    struct libswiftDemangle {
      private init() {}

      static let swift_demangle_getDemangledName = "swift_demangle_getDemangledName"
    }
  }
}
