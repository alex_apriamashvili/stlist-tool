//
//  Mangler.swift
//  
//
//  Created by Alex Apriamashvili on 16/9/19.
//

import Foundation

// MARK: - Mangler

public protocol NameMangling {

  /// path to the temporary mangled symbols storage (file)
  var mangleBin: String { get }

  /// mangle a given binary and put all filtered contents into the temporary file
  /// a path to that file might be obtained for the `mangleBin` property
  /// - parameter _: path to the binary
  /// - parameter filter: a regular expression that helps filtering unnecessary symbols out
  /// - returns: `true` if mangling was successful, otherwise - `false`
  @discardableResult
  func mangle(_: String, filter: String?) -> Bool

  /// mangle a given binary and put all contents into the temporary file
  /// a path to that file might be obtained for the `mangleBin` property
  /// - parameter _: path to the binary
  /// - returns: `true` if mangling was successful, otherwise - `false`
  @discardableResult
  func mangle(_: String) -> Bool
}

// MARK: - Mangler Implementation

public final class XCRunMangler: NameMangling {

  public lazy var mangleBin: String = {
    return "\(file.tempDir)/test_segments"
  }()

  private let shell: ShellGateway
  private let file: FileHelper
  private lazy var xcrunNM = xcrunNMCLI()

  public init(_ shell: ShellGateway, _ file: FileHelper) {
    self.shell = shell
    self.file = file
  }

  @discardableResult
  public func mangle(_ input: String) -> Bool {
    return mangle(input, filter: nil)
  }

  @discardableResult
  public func mangle(_ file: String, filter: String?) -> Bool {
    guard let nm = xcrunNM, !nm.isEmpty else {
      print(C.Message.error)
      return false
    }
    return execute(nm, objectFile: file, filter: filter!)
  }
}

// MARK: - Private

private extension XCRunMangler {

  func xcrunNMCLI() -> String? {
    return shell.executeByPipingBack(C.xcrunPath, C.findOption, C.nmCMD)
  }

  func execute(_ cli: String, objectFile: String, filter: String?) -> Bool {
    var cmd = "\(cli) \"\(objectFile.strip())\" >> \"\(mangleBin)\""
    if let f = filter {
      cmd = "\(cli) \"\(objectFile.strip())\" | grep '\(f)' >> \"\(mangleBin)\""
    }

    return shell.execute(cmd).isSuccess()
  }
}

// MARK: - Contant

private extension XCRunMangler {

  struct C {
    static let xcrunPath = "/usr/bin/xcrun"
    static let findOption = "--find"
    static let nmCMD = "nm"

    struct Message {
      static let error = "No `xcrun nm` tool was found. Please, ensure xctools were installed."
    }
  }
}
