//
//  String.swift
//  stlist
//
//  Created by Alex Apriamashvili on 17/9/19.
//

import Foundation

extension String: Grep {

  public func grep(_ regex: String) -> [String] {
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let results = regex.matches(in: self, range: NSRange(startIndex..., in: self))
      return results.map { String(self[Range($0.range, in: self)!]) }
    } catch {
      print("Invalid RegEx: \(error.localizedDescription)")
      return []
    }
  }
}

extension String: Strip {
  
  public func strip() -> String {
    return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
  }
}

extension Substring: Strip {

  public func strip() -> Substring {
    return Substring(self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
  }
}

extension Substring: ToString {

  public func toString() -> String {
    return String(self)
  }
}

extension String: ToTest {

  public func toTest() -> Test? {
    let separator = Character(".")
    let componentList = self.split(separator: separator)
    guard componentList.count == 3 else { return nil }
    return Test(
      componentList[0].toString(),
      componentList[1].toString(),
      componentList[2].strip().toString()
    )
  }
}
