//
//  Integer.swift
//  stlist
//
//  Created by Alex Apriamashvili on 18/9/19.
//

import Foundation

extension Int32: IsSuccessfulShellExecution {

  public func isSuccess() -> Bool {
    return self == .zero
  }
}
