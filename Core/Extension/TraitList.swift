//
//  TraitList.swift
//  stlist
//
//  Created by Alex Apriamashvili on 17/9/19.
//

import Foundation

/// a trait that defines if a certain entity is eligible for regex mathing
public protocol Grep {

  /// lookup for the matches corresponding to the regular expression in the receiver
  /// - parameter _: regular expression
  /// returns an array of matches or an empty array if no matches found
  func grep(_: String) -> [String]
}

/// a trait that defines if a certain entity is eligible for shell execution result decision making
public protocol IsSuccessfulShellExecution {

  /// check if the shell execution was successfull or not
  func isSuccess() -> Bool
}

/// a trait that defines if a certain entity is eligible for the unification and sorting of it's components
public protocol UniqueSorted {

  /// remove duplicate elements in the receiver
  /// and resurn a sorted collection of those elements
  func uniqueSorted() -> Self
}

/// a trait that defines if a certain entity is eligible for stripping
public protocol Strip {

  /// strip the receiver out of unwanted elements
  func strip() -> Self
}

/// a trait that defines if a certain entity is eligible for converting itself into a `Test`
public protocol ToTest {

  func toTest() -> Test?
}

/// a trait that defines if a certain entity is eligible for converting itself into a `String`
public protocol ToString {

  func toString() -> String
}
