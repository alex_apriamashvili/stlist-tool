//
//  Collection.swift
//  stlist
//
//  Created by Alex Apriamashvili on 17/9/19.
//

import Foundation

extension Array: UniqueSorted where Element == String {

  public func uniqueSorted() -> Array<String> {
    return Set(self).sorted()
  }
}
