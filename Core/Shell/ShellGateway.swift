//
//  ShellGateway.swift
//  
//
//  Created by Alex Apriamashvili on 16/9/19.
//

import Foundation

/// a protocol that describes a gateway to a command line
public protocol ShellGateway {

  /// execute the given command by dispatching a `/bin/sh/` subprocess
  ///
  /// - usage:
  ///     ```
  ///     shell.execute("echo 'hello world'")
  ///     ```
  ///     will print out:
  ///     ```
  ///     hello world
  ///     ```
  ///     in to the system out
  /// - discussion:
  ///
  ///     - this function is encouraged to be used when a certain command just needs to be executed
  ///     without access to the output of that command
  ///     - please, use `executeByPipingBack(_:, _:...) -> String`
  ///     to get an access to the command line output
  ///
  /// - parameter command: the command that needs to be executed in shell
  /// - returns: exit code of the command executed by sh
  func execute(_ command: String) -> Int32

  /// execute the given command as a subprocess and pipes the output of that subprocess back to the programm
  ///
  /// - usage:
  ///     ```
  ///     shell.executeByPipingBack("/usr/bin/xcrun", "--find", "nm")
  ///     ```
  ///     will return:
  ///     ```
  ///     "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/nm"
  ///     ```
  ///     in to the system out
  /// - discussion:
  ///
  ///     - this function is encouraged to be used when an output of a certain command
  ///     needs to be retrieved and handled in your code
  ///     - please, use `execute(_:) -> Int32`
  ///     to execute a command that doesn't imply it's output to be handled in your app
  ///
  /// - parameter cmd: path to the process that needs to be started
  /// - parameter args: list of arguments that needs to be passed to the process
  /// - returns: output of the executed command upon completion
  func executeByPipingBack(_ cmd: String, _ args: String...) -> String
}

public final class ShellGatewayImpl: ShellGateway {

  public init() {}

  public func executeByPipingBack(_ cmd: String, _ args: String...) -> String {
    let shell = Process()
    let pipe = Pipe()

    setExecutable(cmd, for: shell)
    shell.arguments = args
    shell.standardOutput = pipe

    start(shell)
    shell.waitUntilExit()

    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    return String(data: data, encoding: .utf8)?.strip() ?? ""
  }

  public func execute(_ command: String) -> Int32 {
    let task = Process()
    setExecutable("/bin/sh", for: task)
    task.arguments = ["-c", command.strip()]
    start(task)
    task.waitUntilExit()

    return task.terminationStatus
  }
}

private extension ShellGatewayImpl {
  
  func setExecutable(_ path: String, for task: Process) {
    #if os(Linux)
    task.executableURL = URL(string: path)
    #else
    task.launchPath = path
    #endif
  }
  
  func start(_ task: Process) {
    #if os(Linux)
    try? task.run()
    #else
    task.launch()
    #endif
  }
}
