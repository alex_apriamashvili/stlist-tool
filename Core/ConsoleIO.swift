//
//  ConsoleIO.swift
//  stlist
//
//  Created by Alex Apriamashvili on 18/9/19.
//

import Foundation

// MARK: - Input

public struct ConsoleIO {

  public typealias InputParameters = [(key: ConsoleIO.InputParam, value: String)]

  private let loader: ParameterLoader

  public init(_ loader: ParameterLoader = ArgumentLoader.loader()) {
    self.loader = loader
  }

  public func inputParameters() -> InputParameters {
    switch loader.loadParameters() {
    case let .success(parameters):
      return parameters.sorted { $0.key.rawValue < $1.key.rawValue }
    case let .failure(error):
      ConsoleIO.writeMessage(error.localizedDescription, to: .error)
      return []
    }
  }
}

extension ConsoleIO {

  /// an enum that keeps descriptions of all recognizable input parameter keys
  ///
  /// rawValue her represents the priority of each argument,
  /// hence the order in the resulting sorted param dictionary
  /// for example:
  /// `path` has a value of `1`, where `xctestrun` has a value of `2`,
  /// thus is guaranteed that the `path` parameter will be handled before the `xctestrun` one,
  /// regardless on the order of hose params provided in the command line, hece:
  /// ```
  /// stlist -p /path/one --xctestrun /path/two
  /// ```
  /// and
  /// ```
  /// stlist --xctestrun /path/two -p /path/one
  /// ```
  /// will result in the same handling sequence: `-p` is handled the first, and `--xctestrun` the second
  public enum InputParam: String {
    case help = "0"
    case path = "1"
    case xctestrun = "2"
    case output = "99"
    case none

    init(value: String) {
      switch value {
      case Arg.path.short, Arg.path.long: self = .path
      case Arg.help.short, Arg.help.long: self = .help
      case Arg.xctestrun.long: self = .xctestrun
      case Arg.output.short, Arg.output.long: self = .output
      default: self = .none
      }
    }
  }

  struct Arg {

    let short: String
    let long: String

    static let path = Arg(short: "-p", long: "--path")
    static let help = Arg(short: "-h", long: "--help")
    static let xctestrun = Arg(short: "<none>", long: "--xctestrun")
    static let output = Arg(short: "-o", long: "--output")
  }
}

// MARK: - Output

extension ConsoleIO {

  public enum OutputType {
    case error
    case standard
  }

  public static func writeMessage(_ message: String, to: OutputType = .standard) {
    switch to {
    case .standard:
      print("\(message)")
    case .error:
      fputs("Error: \(message)\n", stderr)
    }
  }
}

public enum InputError: Error {

  case emptyArgumentList
  case unevenNumberOfParameters
  case invalidParameterSequence
  case unrecognizedParameter
}


public protocol ParameterLoader {

  typealias ParameterList = [ConsoleIO.InputParam : String]

  func loadParameters() -> Result<ParameterList, InputError>
}

public struct ArgumentLoader: ParameterLoader {

  private let argumentList: [String]
  private let argumentCount: Int32

  public init(_ argList: [String], _ argCount: Int32) {
    argumentList = argList
    argumentCount = argCount
  }

  public static func loader() -> ParameterLoader {
    return ArgumentLoader(CommandLine.arguments, CommandLine.argc)
  }

  public func loadParameters() -> Result<ParameterList, InputError> {
    guard argumentCount > .zero else { return .failure(.emptyArgumentList) }
    guard argumentCount > 1 else { return .success(Parameter.none) }
    let parameters = Array(argumentList.dropFirst())
    guard !needsHelp() else { return .success(Parameter.help) }
    guard isEven(parameters) else { return .failure(.unevenNumberOfParameters) }
    var paramList = ParameterList()
    var metOutput = false
    for i in stride(from: 0, to: parameters.count, by: 2) {
      let key = parameters[i]
      let value = parameters[i + 1]
      guard isValidKey(key) else { return .failure(.invalidParameterSequence) }
      let inputParamKey = ConsoleIO.InputParam(value: key)
      guard inputParamKey != .none else { return .failure(.unrecognizedParameter) }
      if inputParamKey == .output { metOutput = true }
      paramList[inputParamKey] = value
    }
    if metOutput && paramList.count < 2 { return .failure(.invalidParameterSequence) }
    return .success(paramList)
  }
}

extension ArgumentLoader {

  private func needsHelp() -> Bool {
    let set = Set(argumentList.dropFirst())
    return set.contains(ConsoleIO.Arg.help.short) || set.contains(ConsoleIO.Arg.help.long)
  }

  private func isEven<C: Collection>(_ collection: C) -> Bool {
    return collection.count % 2 == .zero
  }

  private func isValidKey(_ key: String) -> Bool {
    return key.contains("-") || key.contains("--")
  }
}

extension ArgumentLoader {

  enum Parameter {
    static let none: ParameterList = [.none : ""]
    static let help: ParameterList = [.help : ""]
  }
}
