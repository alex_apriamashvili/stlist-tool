//
//  Test.swift
//  Core
//
//  Created by Alex Apriamashvili on 1/10/19.
//

import Foundation

/// a structure that describes a single `Unit-` `UI-` `test` entry
public struct Test: Equatable {

  /// test suite, normally represented by a test target (bundle)
  public let suite: String

  /// test case, normally represented by a test class
  public let `case`: String

  /// a particular test method inside a test-case
  /// (the one that starts with `test...`)
  public let method: String

  public init(_ suite: String, _ case: String, _ method: String) {
    self.suite = suite
    self.case = `case`
    self.method = method
  }
}

extension Test: ToString {

  public func toString() -> String {
    return "\(suite).\(`case`).\(method)"
  }
}
