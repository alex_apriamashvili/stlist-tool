load("@build_bazel_rules_apple//apple:macos.bzl", "macos_command_line_application")
load("@build_bazel_rules_apple//apple:macos.bzl", "macos_unit_test")
load("@build_bazel_rules_swift//swift:swift.bzl", "swift_library")

_minimum_os_version = "10.14"

macos_command_line_application(
    name = "stlist",
    bundle_id = "com.alex.apriamashvili.stlist.cli",
    minimum_os_version = _minimum_os_version,
    deps = [":cli"],
)

# - Production Dependencies

swift_library(
    name = "cli",
    srcs = [
        "Sources/stlist/main.swift",
    ],
    deps = [
        ":core",
        ":filter",
    ],
)

swift_library(
    name = "core",
    srcs = glob([
        "Core/*swift",
        "Core/*/*.swift",
    ]),
    module_name = "Core",
)

swift_library(
    name = "filter",
    srcs = glob([
        "Filter/*.swift",
        "Filter/*/*.swift",
    ]),
    module_name = "Filter",
)

# - Tests

swift_library(
    name = "coreTestsLib",
    testonly = 1,
    srcs = glob([
        "CoreTests/*.swift",
        "CoreTests/*/*.swift",
    ]),
    deps = [":core"],
    module_name = "CoreTests",
)

macos_unit_test(
    name = "core_tests",
    minimum_os_version = _minimum_os_version,
    deps = [":coreTestsLib"],
)
