import XCTest

import CoreTests

var tests = [XCTestCaseEntry]()
tests += CoreTests.__allTests()

XCTMain(tests)
